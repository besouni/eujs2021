$("#ex7").click(function (){
    $("#d1").hide(2000).show(2000).fadeOut(2000).fadeIn(2000).slideUp(2000).slideDown(2000);
})

$("#ex4").click(function (){
    $("#d1").hide(2000, function (){console.log("Hidden")});
})

$("#ex5").click(function (){
    $("#d1").show(2000);
})

$("#ex6").click(function (){
    $("#d1").toggle(2000);
})



function example1(){
    var d1 = document.querySelector("#d1")
    console.log(d1)
    var d1_jQ = $("#d1")
    console.log(d1_jQ)
    $("#d1").text("Hello jQuery")
    // console.log($(this))
    // $(this).text("Clicked")
}

// document.getElementById("ex2").addEventListener("click", function (){
//     console.log(this)
// })

$("#ex2").click(function (){
    console.log(this)
    console.log($(this))
})

$("#ex3").on({
    click:function (){
        console.log("click")
    },
    mouseover:function (){
        console.log("over")
    },
    mouseleave: function (){
        console.log("leave")
    }
})