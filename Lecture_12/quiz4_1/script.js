function start(){
    k = setInterval(addBox, 500)
}

var count  = 0;
function addBox(){
    var colors = ["green", "blue", "red", "yellow"]
    var square = document.getElementById("square")
    var box = document.createElement("div")
    var randomColor = Math.floor(Math.random()*4)
    box.style.backgroundColor = colors[randomColor]
    box.classList.add("box")
    square.appendChild(box)
    count++;
    if(count==42){
        clearInterval(k)
    }
}