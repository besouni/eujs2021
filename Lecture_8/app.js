json = {
    name: "Saba",
    age: 21,
    points: [98, 100, 99, 100, 100],
    family:{member1:"father", member2:"mother"},
    f:function (){
        console.log("JSON")
    }
}
console.log(json.name)
console.log(json.points)
console.log(json.points[2])
console.log(json.family.member2)
console.log(json['name'])
console.log(json)
json.lastname = "Jiqidze"
console.log(json)
json.name = "Leqso"
console.log(json)
json.f()