// console.log(document)
// console.log(document.body)
// console.log(document.head)
// console.log(document.body)
// console.log(document.body.children)
// div2 = document.body.children[1]
// console.log(div2)
// div2.classList.add("d2")
// console.log(div2.children)
// console.log(div2.childNodes)
// for (let i=0; i<div2.children.length; i++){
//     console.log(div2.children[i])
// }



function test(){
    var result = document.querySelector("#result")
    console.log(result)
    console.log(result.nextSibling)
    console.log(result.nextElementSibling.nextElementSibling)
    result.nextElementSibling.style.backgroundColor = "green"
    result.previousElementSibling.style.backgroundColor = "yellow"
}

function test1(el){
    console.log(el)
    var parent = el.parentElement
    console.log(parent)
    for(let i=0; i<parent.children.length; i++){
        parent.children[i].classList.add("button")
    }
}

function test2(){
    var table = document.getElementById("table")
    console.log(table)
    console.log(table.rows)
    console.log(table.rows[0].cells)
    for(let i=1; i<table.rows.length; i+=2){
        table.rows[i].classList.add("tr_even")
    }
}

