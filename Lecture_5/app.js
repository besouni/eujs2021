// setTimeout(f1, 3000)
// function f1(){
//     console.log("HTML")
//     setTimeout(f2, 3000)
// }
//
// function f2(){
//     console.log("CSS")
//     setTimeout(f1, 3000)
// }

// f1()

var c1 = 0
function g1(){
    console.log("G1 - HTML - "+c1)
    c1 += 1
}

var c2 = 0
function g2(){
    console.log("G2 - CSS - "+c2)
    c2 += 2
}

var c3 = 0
function g3(){
    console.log("G3 - Javascript - "+c3)
    c3 += 3
    if(c3==6){
        clearInterval(s1)
        clearInterval(s2)
        clearInterval(s3)
    }
}

s1 = setInterval(g1, 3000)
s2 = setInterval(g2, 3000)
s3 = setInterval(g3, 3000)
console.log(s1, s2, s3)
