function test1(){
    var div1_1 = document.getElementById("d1")
    console.log("getElementById - > "+div1_1)
    console.log(div1_1)
    var div1_2 = document.querySelector("#d2")
    console.log("querySelector -> "+div1_2)
    console.log(div1_2)
    var div3_1 =  document.querySelector(".d3")
    console.log(div3_1)
    div1_1.innerText = "<p>Hello HTML</p>"
    div3_1.innerHTML = "<p>Hello HTML</p>"
}

function test2(){
    var div1_1 = document.getElementById("d1")
    div1_1.innerText = "On mouse Over";
}

function test3(){
    var div1_4 = document.getElementById("d4")
    div1_4.innerText = "On Mouse Down"
}

function test4(){
    var div1_4 = document.getElementById("d4")
    div1_4.innerText = "On Mouse Up"
}

function test5(){
    var div1_4 = document.getElementById("d4")
    div1_4.innerText = "On Mouse Click"
}

function test6(){
    var div1_4 = document.getElementById("d4")
    div1_4.innerText = "On Mouse OVER"
}

function test7(){
    var tags = document.getElementsByTagName("div")
    console.log(tags)
    var class_d3 =  document.getElementsByClassName("d3")
    console.log(class_d3)
    console.log(class_d3[0])
    console.log(class_d3['d1'])
}

function homework(){
    var students = ["Kaxa", "Saba"]
    var testResult = document.getElementById("d5")
    var random = Math.floor(Math.random()*2)
    testResult.innerText = students[random]
}

function quiz1_1(){
    var t = "javascript java"
    var text = t;
    console.log(t)
    t = new Set(t)
    console.log(t)
    t = Array.from(t);
    console.log(t)
    t =  t.join("")
    console.log(t)
    console.log(t[0], text)
    console.log(countSymbols(t[0], text))
}

function countSymbols(symbol, t){
    return t.split(symbol).length - 1
}

quiz1_1()