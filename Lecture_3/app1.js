s = "Hello Java Script, JavaScript is a programming language"
console.log(s)
console.log(s[1])
console.log(s[4])
m = [3, 9.4, "javascript", false]
console.log(s.length)
console.log(s.indexOf("Java"))
console.log(s.slice(7))
console.log(s.slice(1, 4)) // [1; 4)
console.log(s.substr(1, 4)) // 1-დან დაწყებული ამოიღებს 4 სიმბოლოს
console.log(s.replace("Java", "Action"))
s1 = "Hello"
s2 = " JavaScropt"
// s3 = s1+s2
s3 = s1.concat(s2)
console.log(s3)
console.log(s1)
s = "Hello"
s = "გამარჯობა"
console.log(s[1])
console.log(s.charAt(1))
console.log(s.charCodeAt(2))
s = "Hello students, javascript it is a good programming language"
console.log(s)
console.log(s.split(","))
m = s.split(" ")
console.log(m)
s1 = m.join(" ")
console.log(s1)
m1 = [4, 5, 5, "hello"]
console.log(m1)
console.log(m1.join(" ---- "))
