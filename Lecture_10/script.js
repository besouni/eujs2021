function start(){
    s = setInterval(add_ball, 500)
}

function stop(){
    clearInterval(s)
}

function add_ball(){
    var ball = document.createElement("div")
    ball.classList.add("ball")
    ball.style.backgroundColor = get_random_color()
    var top = Math.floor(Math.random()*350)
    ball.style.top = top+"px"
    var squere = document.querySelector(".squere")
    var square_width = parseInt(squere.offsetWidth) - 52
    console.log(square_width)
    var left = Math.floor(Math.random()*square_width)
    ball.style.left = left+"px"
    ball.addEventListener("click", function (){
        this.parentElement.removeChild(this)
    })
    squere.appendChild(ball)
}

function get_random_color(){
    var red = Math.floor(Math.random()*255)
    var green = Math.floor(Math.random()*255)
    var blue = Math.floor(Math.random()*255)
    return "rgb("+red+","+green+","+blue+")"
}